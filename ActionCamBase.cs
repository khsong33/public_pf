﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCamBase : MonoBehaviour
{
	public Transform ActionCamLack;
	public Camera ActionCamera;
	public bool Loop = false;
	public delegate void OnFinishedPlayAction(string actionCamName);
	public OnFinishedPlayAction OnFinished;

	public GameObject LookAtObject;

	public virtual void PlayActionCam()
	{
		ActionCamEditor editor = gameObject.GetComponent<ActionCamEditor>();
		if (editor != null)
			Destroy(editor);
	}
	public virtual void StopActionCam() { }
	public virtual Vector3 EditorCamera(float factor) { return Vector3.zero; }
}
