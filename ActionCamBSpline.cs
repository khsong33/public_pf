﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCamBSpline : ActionCamBase
{
	public int n = 2; // Degree of the curve
	public BSplineControlPoint[] controlPoints; // The control points.
	public float Duration = 5.0f;
	public AnimationCurve MoveCurve = new AnimationCurve(new Keyframe(0f, 0f, 0f, 0f), new Keyframe(1f, 1f, 1f, 1f));

	public ActionCamPosModule ActionCamPositionModule;

	private Vector3[] cachedControlPoints; // cached control points
	private int[] nV; // Node vector

	void Start()
	{
		cachedControlPoints = new Vector3[controlPoints.Length];

		CacheControlPoints();

		nV = new int[cachedControlPoints.Length + 5];

		CreateNodeVector();

		ActionCamPositionModule = GetComponent<ActionCamPosModule>();

		if(ActionCamPositionModule != null)
		{
			for (int i = 0; i < controlPoints.Length; i++)
			{
				if (controlPoints[i].IsBattleCamPosition)
				{
					ActionCamPositionModule.AddCamPosition(ActionCamManager.Instance.BattleCamera.MainCamera.transform, controlPoints[i].transform);
				}
			}
			ActionCamPositionModule.UpdateCamPosition();
		}

		ActionCamLack.transform.localPosition = new Vector3(controlPoints[0].transform.localPosition.x,
															controlPoints[0].transform.localPosition.y,
															controlPoints[0].transform.localPosition.z);

		ActionCamera.transform.LookAt(LookAtObject.transform);
	}

	public override void PlayActionCam()
	{
		base.PlayActionCam();
		ActionCamLookAtMover lookatMover = GetComponent<ActionCamLookAtMover>();
		if (lookatMover != null)
			lookatMover.MoveLookatObject();
		StartCoroutine(CoPlayActionCam());
	}

	private IEnumerator CoPlayActionCam()
	{
		if (controlPoints.Length <= 0) yield break;

		cachedControlPoints = new Vector3[controlPoints.Length];

		// Cached the control points
		CacheControlPoints();

		if (cachedControlPoints.Length <= 0) yield break;

		// Initialize node vector.
		nV = new int[cachedControlPoints.Length + 5];
		CreateNodeVector();

		Vector3 camPosition = Vector3.zero;

		float currentTime = 0.0f;
		float factor = 0.0f;

		yield return null;

		while(currentTime <= Duration)
		{
			var val = currentTime / Duration;
			factor = MoveCurve.Evaluate(val);

			if (factor >= 1.0f)
			{
				factor = 1.0f;
			}

			camPosition = EditorCamera(factor);

			if(!(float.IsNaN(camPosition.x) && float.IsNaN(camPosition.y) && float.IsNaN(camPosition.z)))
				ActionCamLack.transform.localPosition = new Vector3(camPosition.x, camPosition.y, camPosition.z);
			ActionCamera.transform.LookAt(LookAtObject.transform);

			currentTime += Time.smoothDeltaTime;
			if (Loop && currentTime >= Duration)
			{
				factor = 0.0f;
				currentTime = currentTime - Duration;
			}

            yield return null;
		}

		// 최후구간 보정
		camPosition = EditorCamera(1.0f);

		if (!(float.IsNaN(camPosition.x) && float.IsNaN(camPosition.y) && float.IsNaN(camPosition.z)))
			ActionCamLack.transform.localPosition = new Vector3(camPosition.x, camPosition.y, camPosition.z);
		ActionCamera.transform.LookAt(LookAtObject.transform);

		if (OnFinished != null)
			OnFinished(name);

		//ActionCamLack.gameObject.SetActive(false);
	}

	public override void StopActionCam()
	{
		base.StopActionCam();
	}

	public override Vector3 EditorCamera(float factor)
	{
		Vector3 cameraPosition = Vector3.zero;
		float i = Mathf.Lerp(0.0f, nV[n + cachedControlPoints.Length], factor);
		if (factor >= 1.0f)
		{
			cameraPosition = controlPoints[controlPoints.Length - 1].transform.localPosition;
		}
			
		else
		{
			for (int j = 0; j < cachedControlPoints.Length; j++)
			{
				if (i >= j)
				{
					cameraPosition = deBoor(n, j, i);
				}
			}
		}
		return cameraPosition;
	}


	// Recursive deBoor algorithm.
	private Vector3 deBoor(int r, int i, float u)
	{
		if (r == 0)
		{
			if (i >= cachedControlPoints.Length)
				i = i - 1;
			return cachedControlPoints[i];
		}
		else
		{
			float pre = (u - nV[i + r]) / (nV[i + n + 1] - nV[i + r]); // Precalculation
			return ((deBoor(r - 1, i, u) * (1 - pre)) + (deBoor(r - 1, i + 1, u) * (pre)));
		}
	}

	private void CreateNodeVector()
	{
		int knoten = 0;

		for (int i = 0; i < (n + cachedControlPoints.Length + 1); i++) // n+m+1 = nr of nodes
		{
			if (i > n)
			{
				if (i <= cachedControlPoints.Length)
				{

					nV[i] = ++knoten;
				}
				else
				{
					nV[i] = knoten;
				}
			}
			else
			{
				nV[i] = knoten;
			}
		}
	}

	private void CacheControlPoints()
	{

		for (int i = 0; i < controlPoints.Length; i++)
		{
			cachedControlPoints[i] = controlPoints[i].cachedPosition;
		}

	}


#if UNITY_EDITOR
	void OnDrawGizmos()
	{
		if (controlPoints.Length <= 0) return;

		cachedControlPoints = new Vector3[controlPoints.Length];

		// Cached the control points
		CacheControlPoints();

		if (cachedControlPoints.Length <= 0) return;

		// Initialize node vector.
		nV = new int[cachedControlPoints.Length + 5];
		CreateNodeVector();


		// Draw the bspline lines
		Gizmos.color = Color.gray;

		Vector3 start = cachedControlPoints[0];
		Vector3 end = Vector3.zero;

		for (float i = 0.0f; i < nV[n + cachedControlPoints.Length]; i += 0.02f)
		{
			for (int j = 0; j < cachedControlPoints.Length; j++)
			{
				if (i >= j)
				{
					end = deBoor(n, j, i);
				}
			}
			Gizmos.DrawLine(start, end);
			start = end;
		}
	}
#endif
}
