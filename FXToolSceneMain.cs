﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maestro;

#if UNITY_EDITOR
using UnityEditor;
using UnityEngine.EventSystems;
using UnityEditor.SceneManagement;

[Serializable]
public class FxToolEffectInputData
{
	public InputField _fxPrefabName;
	public InputField _fxLifeTime;
	public InputField _fxStartTime;
	public InputField _fxStartAnimName;
	public InputField _fxRootName;

	[HideInInspector]
	public bool _isFxOn = false;
	[HideInInspector]
	public GameObject _currentFxGo = null;
	[HideInInspector]
	public float _currentFxTime = 0.0f;
	[HideInInspector]
	public float _currentFxStartTime = 0.0f;
	[HideInInspector]
	public ParticleSystem _currentParticleSystem = null;
}

public enum ToolObjectType
{
	Unit,
	Titan
}

public class FxToolEffectPlayData
{
	public string _fxPrefabName;
	public float _fxLifeTime;
	public float _fxStartTime;
	public string _fxStartAnimName;
	public string _fxRootName;

	public bool _isFxOn = false;
	public GameObject _currentFxGo = null;
	public ParticleSystem _currentParticleSystem = null;
	public float _prevParticleTime = 0.0f;
}

public class FxToolButtonData
{
	public string AnimStateName;
	public float FxLifeTime;
	public float FxStartTime;
	public string FxPrefabName;
	public string FxRootName;
}

public class FxToolProjectileData
{
	public string animationName;
	public string skillName;
	public float delay;
	public GameObject projectileObj;
}

public class FxToolProjectileButtonData
{
	public string AnimName;
	public string PrefabName;
}

public class FXToolSceneMain : MonoBehaviour
{
	#region Heros / Units / Titans
	[SerializeField]
	private Dropdown _heroDropdown;
	[SerializeField]
	private Button _heroSpawnButton;
	[SerializeField]
	private Dropdown _unitDropdown;
	[SerializeField]
	private Button _unitSpawnButton;
	[SerializeField]
	private Dropdown _titanDropdown;
	[SerializeField]
	private Button _titanSpawnButton;
	#endregion

	#region Units
	[SerializeField]
	private GameObject _animationButtonRoot;
	[SerializeField]
	private Button _animationButton;

	[SerializeField]
	private GameObject _animationTimeGo;
	[SerializeField]
	private Slider _animationSlider;
	[SerializeField]
	private Text _animationTime;
	[SerializeField]
	private Text _animationPlayButton;
	#endregion
	#region Fx
	[SerializeField]
	private FxToolEffectInputData _toolFxDatas;
	[SerializeField]
	private GameObject _fxButtonRoot;
	[SerializeField]
	private Button _fxListButton;
	[SerializeField]
	private GameObject _fxMarkerRoot;
	[SerializeField]
	private Image _fxTimeingMakerPrefab;
	#endregion

	#region Animation List
	[SerializeField]
	private Text _animationPlayListTextView;
	#endregion


	[SerializeField]
	private Transform _target;
	[SerializeField]
	private Transform _moveTarget;
	[SerializeField]
	private float _keyInputSpeed = 5.0f;

	[SerializeField]
	private InputField _projectileAnimationName;
	[SerializeField]
	private InputField _projectilePrefabName;
	[SerializeField]
	private GameObject _projectileButtonRoot;
	[SerializeField]
	private Button _projectileListButton;

	private ToolObjectType _toolObjType;

	private int _currentHeroIndex = -1;
	private int _currentUnitIndex = -1;
	private int _currentTitanIndex = -1;

	private UnitData _currentUnitData = null;
	private TitanData _currentTitanData = null;
	private GameObject _currentGo = null;
	private Animator _currentAnimator;
	private float _currentAnimLength = 0f;
	private bool _isPlay = false;

	private float _currentAnimationTime = 0.0f;
	private int _currentAnimationLoopCount = 0;
	private int _prevAnimationLoopCount = -1;

	private E_UnitAnimationState _animationState;
	private E_TitanAnimationState _titanAnimState;

	private UnitBaseTransformation _unitBaseTransform;

	private List<FxToolEffectPlayData> _fxPlayDataList;
	private List<GameObject> _fxButtonObjectList;
	private List<FxToolButtonData> _fxButtonDataList;
	private List<Image> _fxTimeingMakerList;

	private List<string> _animationPlayList;

	#region skill (projectile)
	private List<SkillData> _skillDataList;
	private List<FxToolProjectileData> _skillProjectileLinkAnimationList;
	#endregion

	#region Projectile Tool
	private List<FxToolProjectileData> _projectileDataList;
	private List<GameObject> _projectileButtonObjectList;
	private List<FxToolProjectileButtonData> _projectileButtonList;
	#endregion

	private void Awake()
	{
		Databases.ParsingLatestDatabases();
		SoundController.Instance.Create();
		InitDropdown();
		InitTitanDropDown();

		_fxButtonObjectList = new List<GameObject>();
		_fxButtonDataList = new List<FxToolButtonData>();
		_fxTimeingMakerList = new List<Image>();
		_fxPlayDataList = new List<FxToolEffectPlayData>();
		_animationPlayList = new List<string>();

		_projectileButtonObjectList = new List<GameObject>();
		_projectileDataList = new List<FxToolProjectileData>();
		_projectileButtonList = new List<FxToolProjectileButtonData>();

		_animationPlayListTextView.text = string.Empty;
	}

	private void LateUpdate()
	{
		SetAnimationTimeBar();


		if(_currentGo != null)
		{
			_currentGo.transform.position = new Vector3(_moveTarget.position.x, _currentGo.transform.position.y, _moveTarget.position.z);
		}
		//if (_currentGo != null && _unitBaseTransform != null)
		//{
			////_unitBaseTransform.SetAnimationState(_animationState);
			//if (_currentUnitData != null)
			//{
			//	_currentUnitData.FSMData.Target = new TargetPosition(FVector2D.Create(_target.position.x, _target.position.z));
			//	_currentUnitData.MovementData.CurrentPos = FVector2D.Create(_moveTarget.position.x, _moveTarget.position.z);//new GridPos(_moveTarget.position);
			//}
			//SetUnitAngle();
			//_unitBaseTransform.UpdateTransform();

			//CheckAnimationLoopCount();
		//}
		UpdateKeyCode();
		UpdateFx();
	}

	private void UpdateKeyCode()
	{
		if (Input.GetKey(KeyCode.W))
		{
			_moveTarget.position = new Vector3(_moveTarget.position.x - _keyInputSpeed * Time.deltaTime, 0.0f, _moveTarget.position.z);
		}
		if (Input.GetKey(KeyCode.S))
		{
			_moveTarget.position = new Vector3(_moveTarget.position.x + _keyInputSpeed * Time.deltaTime, 0.0f, _moveTarget.position.z);
		}
		if (Input.GetKey(KeyCode.A))
		{
			_moveTarget.position = new Vector3(_moveTarget.position.x, 0.0f, _moveTarget.position.z - _keyInputSpeed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.D))
		{
			_moveTarget.position = new Vector3(_moveTarget.position.x, 0.0f, _moveTarget.position.z + _keyInputSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.P))
		{
			OnClickSuperPlaySound();
		}
        if (Input.GetKey(KeyCode.H))
        {
            OnClickSelectHero();
        }

    }

	private void CheckAnimationLoopCount()
	{
		AnimatorStateInfo stateInfo = _currentAnimator.GetCurrentAnimatorStateInfo(0);
		if (stateInfo.loop == false)
		{
			_currentAnimationLoopCount = 1;
		}
		else
		{
			_currentAnimationLoopCount = Mathf.CeilToInt(stateInfo.normalizedTime);
		}
		if (_prevAnimationLoopCount != _currentAnimationLoopCount)
		{
			_prevAnimationLoopCount = _currentAnimationLoopCount;
			UpdateAnimationLoopCount();
		}
	}

	private void UpdateAnimationLoopCount()
	{
		for (int i = 0; i < _fxPlayDataList.Count; i++)
		{
			if (_fxPlayDataList[i]._currentFxGo != null)
				_fxPlayDataList[i]._isFxOn = true;
		}
	}

	private void UpdateFx()
	{
		if (_fxPlayDataList == null || _fxPlayDataList.Count <= 0)
			return;

		for (int i = 0; i < _fxPlayDataList.Count; i++)
		{
			if (_fxPlayDataList[i]._isFxOn == false)
				continue;
			if (_currentAnimationTime >= _fxPlayDataList[i]._fxStartTime)
			{
				UnityEngine.Object fxObj = AssetBundleManager.Instance.LoadAsset<UnityEngine.Object>("effects", _fxPlayDataList[i]._fxPrefabName);
				_fxPlayDataList[i]._currentFxGo = PrefabUtility.InstantiatePrefab(fxObj) as GameObject;
				List<Transform> fxRootTransformList = new List<Transform>(_currentGo.transform.GetComponentsInChildren<Transform>());
				Transform fxRootTransform = fxRootTransformList.Find(x => x.name.Equals(_fxPlayDataList[i]._fxRootName));
				if (fxRootTransform == null)
				{
					_fxPlayDataList[i]._currentFxGo.transform.position = (_currentGo != null) ? _currentGo.transform.position : Vector3.zero;
					_fxPlayDataList[i]._currentFxGo.transform.localRotation = (_currentGo != null) ? _currentGo.transform.localRotation : Quaternion.identity;
				}
				else
				{
					_fxPlayDataList[i]._currentFxGo.transform.parent = fxRootTransform;
					_fxPlayDataList[i]._currentFxGo.transform.localPosition = Vector3.zero;
					_fxPlayDataList[i]._currentFxGo.transform.localRotation = Quaternion.identity;
				}
				_fxPlayDataList[i]._currentFxGo.transform.localScale = Vector3.one;

				AutoDestroyTimer destroyTimer = _fxPlayDataList[i]._currentFxGo.AddComponent<AutoDestroyTimer>();
				destroyTimer.lifetime = _fxPlayDataList[i]._fxLifeTime;

				_fxPlayDataList[i]._currentParticleSystem = _fxPlayDataList[i]._currentFxGo.GetComponent<ParticleSystem>();

				_fxPlayDataList[i]._isFxOn = false;
			}
		}
	}

	private void SetAnimationTimeBar()
	{
		if (_currentGo == null || _currentAnimator == null)
		{
			_animationTimeGo.SetActive(false);
			return;
		}

		_animationTimeGo.gameObject.SetActive(true);

		AnimatorStateInfo state = _currentAnimator.GetCurrentAnimatorStateInfo(0);
		if (state.loop)
			_animationSlider.value = state.normalizedTime - (int)state.normalizedTime;
		else
			_animationSlider.value = Mathf.Min(state.normalizedTime, 1f);

		_currentAnimLength = float.IsInfinity(state.length) ? _currentAnimLength : state.length;
		_currentAnimationTime = _animationSlider.value * _currentAnimLength;
		_animationTime.text = string.Format("{0:F3} / {1:F3}", _animationSlider.value * _currentAnimLength, _currentAnimLength);
	}

	private void InitDropdown()
	{
		List<UnitInfo> infoList = Databases.UnitDB.FindConditions(d => d.IsHero && d.Enable);
		List<Dropdown.OptionData> optionData = new List<Dropdown.OptionData>();
		optionData.Add(new Dropdown.OptionData("None"));
		for (int i = 0; i < infoList.Count; i++)
		{
			optionData.Add(new Dropdown.OptionData(infoList[i].UnitID));
		}

		_heroDropdown.ClearOptions();
		_heroDropdown.AddOptions(optionData);

		_heroDropdown.onValueChanged.AddListener(OnHeroIndexChanged);

		infoList = Databases.UnitDB.FindConditions(d => !d.IsHero && d.Enable);
		optionData = new List<Dropdown.OptionData>();
		optionData.Add(new Dropdown.OptionData("None"));
		for (int i = 0; i < infoList.Count; i++)
		{
			optionData.Add(new Dropdown.OptionData(infoList[i].UnitID));
		}

		_unitDropdown.ClearOptions();
		_unitDropdown.AddOptions(optionData);

		_unitDropdown.onValueChanged.AddListener(OnUnitIndexChanged);
	}

	private void InitTitanDropDown()
	{
		List<TitanInfo> infoList = Databases.TitanDB.FindConditions(d => d.Enable);
		List<Dropdown.OptionData> optionData = new List<Dropdown.OptionData>();
		optionData.Add(new Dropdown.OptionData("None"));
		for(int i = 0; i < infoList.Count; i++)
		{
			optionData.Add(new Dropdown.OptionData(infoList[i].TitanId));
		}

		_titanDropdown.ClearOptions();
		_titanDropdown.AddOptions(optionData);

		_titanDropdown.onValueChanged.AddListener(OnTitanIndexChanged);
	}

	private void ClearObject()
	{
		if (_currentGo != null)
		{
			Destroy(_currentGo);
			_currentGo = null;
		}
	}

	private void CreateAnimationButton()
	{
		int i = 0;
		GameObject button = null;
		HashSet<string> animNameChecker = new HashSet<string>();
		foreach (var anim in _currentAnimator.runtimeAnimatorController.animationClips)
		{
			if (animNameChecker.Contains(anim.name))
				continue;

			button = Instantiate(_animationButton.gameObject);
			button.transform.parent = _animationButtonRoot.transform;
			button.SetActive(true);
			button.transform.localScale = Vector3.one;
			button.GetComponent<RectTransform>().anchoredPosition = new Vector3(-16.8f, 279 - 30 * i, 0);
			button.name = anim.name;
			button.GetComponentInChildren<Text>().text = anim.name;

			animNameChecker.Add(anim.name);

			i++;
		}
	}

	private void CreateTitanAnimationButton()
	{
		int i = 0;
		GameObject button = null;
		HashSet<string> animNameChecker = new HashSet<string>();
		foreach (var anim in _currentAnimator.runtimeAnimatorController.animationClips)
		{
			if (animNameChecker.Contains(anim.name))
				continue;
			button = Instantiate(_animationButton.gameObject);
			button.transform.parent = _animationButtonRoot.transform;
			button.SetActive(true);
			button.transform.localScale = Vector3.one;
			button.GetComponent<RectTransform>().anchoredPosition = new Vector3(-16.8f, 279 - 30 * i, 0);
			button.name = anim.name;
			button.GetComponentInChildren<Text>().text = anim.name;

			animNameChecker.Add(anim.name);

			i++;
		}
	}

	private void AnimationPlay(bool play)
	{
		if (_currentAnimator == null)
			return;

		_isPlay = play;

		if (_isPlay)
		{
			_currentAnimator.speed = 1;
			_animationPlayButton.text = "Pause";
		}
		else
		{
			_currentAnimator.speed = 0;
			_animationPlayButton.text = "Play";
		}
	}

	#region Event Handle
	private void OnHeroIndexChanged(int index)
	{
		_currentHeroIndex = index;
	}

	private void OnUnitIndexChanged(int index)
	{
		_currentUnitIndex = index;
	}

	private void OnTitanIndexChanged(int index)
	{
		_currentTitanIndex = index;
	}

	public void OnCreateTitan()
	{
		_toolObjType = ToolObjectType.Titan;
		int index = _currentTitanIndex;
		string id = _titanDropdown.options[index].text;

		FVector2D anglePoint = FVector2D.Create(_target.position.x, _target.position.z);
		int gridAngle = MaestroMath.GetAngle(FVector2D.zero, anglePoint);

		TitanInitializeData initData = new TitanInitializeData(0, 0, E_Team.Home, E_PlayerIndex.Player1, id, FVector2D.zero, gridAngle, 1, 1);
		//_currentUnitData = new UnitData(id, E_Team.Home, E_PlayerIndex.Player1);
		_currentTitanData = new TitanData(initData);

		if (_currentTitanData.LogicData.Info == null)
			return;

		ClearObject();

		string assetName = AssetUtils.GetTitanName(false, _currentTitanData.LogicData.Info.Sprites[0], 1);
		UnityEngine.Object obj = AssetBundleManager.Instance.LoadAsset<UnityEngine.Object>("titans", assetName);
		_currentGo = PrefabUtility.InstantiatePrefab(obj) as GameObject;
		AddFxFromComp(_currentGo.GetComponent<ObjectAnimEffectComp>());
		//_currentGo.GetComponent<UnitObject>().SetData(_currentUnitData, false);
		_currentAnimator = _currentGo.GetComponentInChildren<Animator>();
		ObjectData data = new ObjectData(_currentGo);
		//_unitBaseTransform = _currentGo.GetComponent<UnitBaseTransformation>();
		//_unitBaseTransform.SetData(_currentUnitData, data);

		Selection.activeGameObject = _currentAnimator.gameObject;

		CreateTitanAnimationButton();
		AnimationPlay(true);
		SetProjectileData(_currentTitanData.SkillData.GetSkillList, _currentTitanData.SkillData.ActiveSkill);

		AddProjectileFromComp(_currentGo.GetComponent<ObjectAnimProjectileComp>());
	}

	public void OnCreate(bool isHero)
	{
		_toolObjType = ToolObjectType.Unit;

		int index = isHero ? _currentHeroIndex : _currentUnitIndex;
		if (index <= 0)
			return;

		Dropdown dropdown = isHero ? _heroDropdown : _unitDropdown;
		string id = dropdown.options[index].text;

		FVector2D anglePoint = FVector2D.Create(_target.position.x, _target.position.z);
		int gridAngle = MaestroMath.GetAngle(FVector2D.zero, anglePoint);

        UnitInitializeData unitInitData = new UnitInitializeData(0, E_Team.Home, E_PlayerIndex.Player1, id, FVector2D.zero, gridAngle, 1, 1);
		//_currentUnitData = new UnitData(id, E_Team.Home, E_PlayerIndex.Player1);
		_currentUnitData = new UnitData(unitInitData);

		if (_currentUnitData.Info == null)
			return;

		ClearObject();

		string assetName = AssetUtils.GetUnitName(_currentUnitData.Info.IsHero, _currentUnitData.Info.Sprites[0], 1);
		UnityEngine.Object obj = AssetBundleManager.Instance.LoadAsset<UnityEngine.Object>(assetName, assetName);
		_currentGo = PrefabUtility.InstantiatePrefab(obj) as GameObject;
		AddFxFromComp(_currentGo.GetComponent<ObjectAnimEffectComp>());
		//_currentGo.GetComponent<UnitObject>().SetData(_currentUnitData, false);
		_currentAnimator = _currentGo.GetComponentInChildren<Animator>();
		ObjectData data = new ObjectData(_currentGo);
		_unitBaseTransform = _currentGo.GetComponent<UnitBaseTransformation>();
		_unitBaseTransform.SetData(_currentUnitData, data);

		Selection.activeGameObject = _currentAnimator.gameObject;

		CreateAnimationButton();
		AnimationPlay(true);
		SetProjectileData(_currentUnitData.SkillData.GetSkillList, _currentUnitData.SkillData.ActiveSkill);

		AddProjectileFromComp(_currentGo.GetComponent<ObjectAnimProjectileComp>());
	}

	private void SetProjectileData(List<SkillData> skillList, SkillData activeSkill)
	{
		if (_skillDataList != null)
			_skillDataList.Clear();
		_skillDataList = skillList;

		if (_skillProjectileLinkAnimationList == null)
			_skillProjectileLinkAnimationList = new List<FxToolProjectileData>();
		_skillProjectileLinkAnimationList.Clear();

		foreach (var skill in _skillDataList)
		{
			//if (_currentUnitData == null || _currentUnitData.Info.RangeType != E_RangeType.Range)
			//	continue;
			FxToolProjectileData projectileData = new FxToolProjectileData();
			projectileData.animationName = (skill.Info.AttackAnimation + "_end").ToLower();
			projectileData.delay = skill.Info.CastingTick * BattleConfig.TickTime;
			projectileData.skillName = skill.Info.Id;
			// 차후 component 자동 연결로 수정
			string projectileObjectName = string.Empty;
			//AssetUtils.GetProjectileName(skill.Info.ProjectileType);
			ObjectAnimProjectileComp projectileComp = _currentGo.GetComponent<ObjectAnimProjectileComp>();
			if (projectileComp == null)
			{
				projectileObjectName = AssetUtils.GetProjectileName(skill.Info.ProjectileType);
			}
			else
			{
				projectileObjectName = projectileComp.GetPrefabName(skill.Info.AttackAnimation);
				if(string.IsNullOrEmpty(projectileObjectName))
				{
					projectileObjectName = AssetUtils.GetProjectileName(skill.Info.ProjectileType);
				}
			}
			projectileData.projectileObj = AssetBundleManager.Instance.LoadAsset<GameObject>("projectiles", projectileObjectName);

			_skillProjectileLinkAnimationList.Add(projectileData);
		}
		// Active Skill
		if(activeSkill != null)
		{
			FxToolProjectileData projectileData = new FxToolProjectileData();
			projectileData.animationName = (activeSkill.Info.AttackAnimation + "_end").ToLower();
			projectileData.delay = activeSkill.Info.CastingTick * BattleConfig.TickTime;
			projectileData.skillName = activeSkill.Info.Id;

			string projectileObjectName = string.Empty;
			//AssetUtils.GetProjectileName(skill.Info.ProjectileType);
			ObjectAnimProjectileComp projectileComp = _currentGo.GetComponent<ObjectAnimProjectileComp>();
			if (projectileComp == null)
			{
				projectileObjectName = AssetUtils.GetProjectileName(activeSkill.Info.ProjectileType);
			}
			else
			{
				projectileObjectName = projectileComp.GetPrefabName(activeSkill.Info.AttackAnimation);
				if (string.IsNullOrEmpty(projectileObjectName))
				{
					projectileObjectName = AssetUtils.GetProjectileName(activeSkill.Info.ProjectileType);
				}
			}
			projectileData.projectileObj = AssetBundleManager.Instance.LoadAsset<GameObject>("projectiles", projectileObjectName);

			_skillProjectileLinkAnimationList.Add(projectileData);


		}
	}

	public void CreateFx()
	{
		for (int i = 0; i < _fxTimeingMakerList.Count; i++)
		{
			Destroy(_fxTimeingMakerList[i].gameObject);
		}
		_fxTimeingMakerList.Clear();

		for (int i = 0; i < _fxPlayDataList.Count; i++)
		{
			_fxPlayDataList[i]._isFxOn = true;

			RectTransform animationSliderRect = _animationSlider.GetComponent<RectTransform>();
			float fxstartTimeValue = _fxPlayDataList[i]._fxStartTime / _currentAnimLength;

			GameObject marker = Instantiate<GameObject>(_fxTimeingMakerPrefab.gameObject);
			Image markerImage = marker.GetComponent<Image>();
			markerImage.color = new Color(UnityEngine.Random.Range(0.0f, 1.0f), UnityEngine.Random.Range(0.0f, 1.0f), UnityEngine.Random.Range(0.0f, 1.0f));
			marker.transform.parent = _fxMarkerRoot.transform;
			marker.SafeSetActive(true);

			RectTransform fxMarkTransform = marker.GetComponent<RectTransform>();
			fxMarkTransform.anchoredPosition3D = new Vector3(animationSliderRect.rect.width * fxstartTimeValue, 25.0f, 0.0f);

			_fxTimeingMakerList.Add(markerImage);
		}
	}

	public void AddFxFromComp(ObjectAnimEffectComp comp)
	{
		for (int i = 0; i < _fxButtonObjectList.Count; i++)
		{
			Destroy(_fxButtonObjectList[i]);
		}
		_fxButtonObjectList.Clear();
		_fxButtonDataList.Clear();

		if (comp == null)
			return;
		if (comp.AllDatas == null)
			return;

		for (int i = 0; i < comp.AllDatas.Count; i++)
		{
			FxToolButtonData buttonData = new FxToolButtonData();
			buttonData.AnimStateName = comp.AllDatas[i].FxAnimationName.ToLower();
			buttonData.FxStartTime = comp.AllDatas[i].FxStartTime;
			buttonData.FxLifeTime = comp.AllDatas[i].FxLifeTime;
			buttonData.FxPrefabName = comp.AllDatas[i].FxPrefabName;
			buttonData.FxRootName = comp.AllDatas[i].FxRootTransformName;

			AddFxButton(buttonData);
		}
	}

	public void AddProjectileFromComp(ObjectAnimProjectileComp comp)
	{
		for(int i = 0; i < _projectileButtonObjectList.Count; i++)
		{
			Destroy(_projectileButtonObjectList[i]);
		}
		_projectileButtonObjectList.Clear();
		_projectileDataList.Clear();

		if (comp == null)
			return;
		if (comp.DataList == null)
			return;

		for(int i = 0; i < comp.DataList.Count; i++)
		{
			FxToolProjectileButtonData buttonData = new FxToolProjectileButtonData();
			buttonData.AnimName = comp.DataList[i].AnimationName;
			buttonData.PrefabName = comp.DataList[i].PrefabName;

			AddProjectileButton(buttonData);
		}
	}

	public void OnAddFx()
	{
		FxToolButtonData buttonData = new FxToolButtonData();
		buttonData.AnimStateName = _toolFxDatas._fxStartAnimName.text.ToLower();
		float.TryParse(_toolFxDatas._fxStartTime.text, out buttonData.FxStartTime);
		float.TryParse(_toolFxDatas._fxLifeTime.text, out buttonData.FxLifeTime);
		buttonData.FxPrefabName = _toolFxDatas._fxPrefabName.text;
		buttonData.FxRootName = _toolFxDatas._fxRootName.text;

		AddFxButton(buttonData);
	}

	private void AddFxButton(FxToolButtonData buttonData)
	{
		GameObject button = Instantiate(_fxListButton.gameObject);
		button.transform.parent = _fxButtonRoot.transform;
		RectTransform rootTransform = _fxButtonRoot.GetComponent<RectTransform>();
		button.SetActive(true);
		button.transform.localScale = Vector3.one;
		button.GetComponent<RectTransform>().anchoredPosition = new Vector3(-17.2f, -45 - 95 * _fxButtonDataList.Count, 0);
		button.name = string.Format("{0}", _fxButtonDataList.Count);
		string fxButtonInfo = GetButtonName(buttonData);
		button.GetComponentInChildren<Text>().text = fxButtonInfo;

		_fxButtonObjectList.Add(button);
		_fxButtonDataList.Add(buttonData);

		rootTransform.sizeDelta = new Vector2(0, 95 * _fxButtonDataList.Count);
	}

	public void OnDeleteFxData(Button deleteButton)
	{
		int index = int.Parse(deleteButton.transform.parent.name);
		_fxButtonDataList.RemoveAt(index);

		for (int i = 0; i < _fxButtonObjectList.Count; i++)
		{
			Destroy(_fxButtonObjectList[i]);
		}
		_fxButtonObjectList.Clear();

		for (int i = 0; i < _fxButtonDataList.Count; i++)
		{
			FxToolButtonData buttonData = _fxButtonDataList[i];
			GameObject button = Instantiate(_fxListButton.gameObject);
			button.transform.parent = _fxButtonRoot.transform;
			RectTransform rootTransform = _fxButtonRoot.GetComponent<RectTransform>();
			button.SetActive(true);
			button.transform.localScale = Vector3.one;
			button.GetComponent<RectTransform>().anchoredPosition = new Vector3(-17.2f, -45 - 95 * i, 0);
			button.name = string.Format("{0}", i);
			string fxButtonInfo = GetButtonName(buttonData);
			button.GetComponentInChildren<Text>().text = fxButtonInfo;

			_fxButtonObjectList.Add(button);
			rootTransform.sizeDelta = new Vector2(0, 95 * _fxButtonDataList.Count);
		}
	}

	public void OnAddProjectile()
	{
		FxToolProjectileButtonData buttonData = new FxToolProjectileButtonData();
		buttonData.AnimName = _projectileAnimationName.text;
		buttonData.PrefabName = _projectilePrefabName.text;

		AddProjectileButton(buttonData);
	}

	private void AddProjectileButton(FxToolProjectileButtonData buttonData)
	{
		GameObject button = Instantiate(_projectileListButton.gameObject);
		button.transform.parent = _projectileButtonRoot.transform;
		RectTransform rootTransform = _projectileButtonRoot.GetComponent<RectTransform>();
		button.SetActive(true);
		button.transform.localScale = Vector3.one;
		button.GetComponent<RectTransform>().anchoredPosition = new Vector3(65.0f, -30 - 65 * _projectileButtonList.Count, 0);
		button.name = string.Format("{0}", _projectileButtonObjectList.Count);
		string buttonInfo = GetProjectileButtonName(buttonData);
		button.GetComponentInChildren<Text>().text = buttonInfo;

		_projectileButtonObjectList.Add(button);
		_projectileButtonList.Add(buttonData);

		rootTransform.sizeDelta = new Vector2(0, 65 * _projectileButtonList.Count);
	}

	public void OnDeleteProjectileData(Button deleteButton)
	{
		int index = int.Parse(deleteButton.transform.parent.name);
		_projectileButtonList.RemoveAt(index);

		for (int i = 0; i < _projectileButtonObjectList.Count; i++)
		{
			Destroy(_projectileButtonObjectList[i]);
		}
		_projectileButtonObjectList.Clear();

		for (int i = 0; i < _projectileButtonList.Count; i++)
		{
			FxToolProjectileButtonData buttonData = _projectileButtonList[i];
			GameObject button = Instantiate(_projectileListButton.gameObject);
			button.transform.parent = _projectileButtonRoot.transform;
			RectTransform rootTransform = _projectileButtonRoot.GetComponent<RectTransform>();
			button.SetActive(true);
			button.transform.localScale = Vector3.one;
			button.GetComponent<RectTransform>().anchoredPosition = new Vector3(65.0f, -30 - 65 * i, 0);
			button.name = string.Format("{0}", i);
			string buttonInfo = GetProjectileButtonName(buttonData);
			button.GetComponentInChildren<Text>().text = buttonInfo;

			_projectileButtonObjectList.Add(button);
			rootTransform.sizeDelta = new Vector2(0, 65 * _projectileButtonList.Count);
		}
	}

	public void OnClickProjetileData(Button button)
	{
		Text projectileText = button.GetComponentInChildren<Text>();
		string[] dataStrings = projectileText.text.Split('\t');
		_projectileAnimationName.text = dataStrings[0];
		_projectilePrefabName.text = dataStrings[1];
	}

	private string GetButtonName(FxToolButtonData buttonData)
	{
		return string.Format("{0}\t{1:F3}\t{2}\t{3:F3}\t{4}", buttonData.AnimStateName, buttonData.FxStartTime, buttonData.FxPrefabName, buttonData.FxLifeTime, buttonData.FxRootName);
	}

	private string GetProjectileButtonName(FxToolProjectileButtonData buttonData)
	{
		return string.Format("{0}\t{1}", buttonData.AnimName, buttonData.PrefabName);
	}

	public void OnAddAnimationList(Button aniButton)
	{
		string animationName = aniButton.transform.parent.name;
		_animationPlayList.Add(animationName);
		_animationPlayListTextView.text = string.Empty;
		for (int i = 0; i < _animationPlayList.Count; i++)
		{
			if (i == 0)
				_animationPlayListTextView.text += _animationPlayList[i];
			else
				_animationPlayListTextView.text += string.Format(" => {0}", _animationPlayList[i]);
		}
	}

	public void OnPlayAnimationList()
	{
		StartCoroutine("CoPlayAnimationList");
	}

	private IEnumerator CoPlayAnimationList()
	{
		if (_animationPlayList == null || _animationPlayList.Count <= 0)
			yield break;
        if (_toolObjType == ToolObjectType.Unit)
            PlayAnimation(_animationPlayList[0], ref _animationState);
        else
            PlayAnimation(_animationPlayList[0], ref _titanAnimState);

		int currentIndex = 0;
		while (currentIndex < _animationPlayList.Count)
		{
			AnimatorStateInfo state = _currentAnimator.GetCurrentAnimatorStateInfo(0);
			if (state.normalizedTime >= 1.0f)
			{
				currentIndex++;
				if (currentIndex < _animationPlayList.Count)
				{
                    if (_toolObjType == ToolObjectType.Unit)
                        PlayAnimation(_animationPlayList[currentIndex], ref _animationState);
                    else
                        PlayAnimation(_animationPlayList[currentIndex], ref _titanAnimState);
				}
			}
			yield return null;
		}
	}

	public void OnClearAnimationList()
	{
		_animationPlayList.Clear();
		_animationPlayListTextView.text = string.Empty;
	}

	public void OnClickAnimation(Button button)
	{
        if (_toolObjType == ToolObjectType.Unit)
            PlayAnimation(button.name, ref _animationState);
		else
            PlayAnimation(button.name, ref _titanAnimState);
    }

    public void PlayAnimation<T>(string animationName, ref T animState)
    {
        if (_currentAnimator == null)
            return;

        animationName = animationName.ToLower();

        //if (_currentAnimator.HasState(0, Animator.StringToHash(animationName)))
        //{
        //    _currentAnimator.Play(animationName.ToLower(), 0, 0);
        //}
        //else
        //{
        //    if (animationName.EndsWith("_end"))
        //    {
        //        animationName = animationName.Substring(0, animationName.LastIndexOf("_end"));
        //        _currentAnimator.Play(animationName.ToLower(), 0, 0);
        //    }
        //}
        //if (animationName.EndsWith("_start"))
        //    animationName = animationName.Substring(0, animationName.LastIndexOf("_start"));

        if (_currentAnimator.HasState(0, Animator.StringToHash(animationName)))
            _currentAnimator.Play(animationName.ToLower(), 0, 0);

        string[] states = Enum.GetNames(typeof(T));
        Array values = Enum.GetValues(typeof(T));
        animState = (T)values.GetValue(0);
        for (int i = 0; i < states.Length; i++)
        {
            if (states[i].ToLower().Equals(animationName.ToLower()))
            {
                animState = (T)values.GetValue(i);
            }
        }

        AnimationPlay(true);
        _currentAnimationLoopCount = 0;
        _prevAnimationLoopCount = -1;

        SetFxData(animationName);
        StartProjectile(animationName);
        PlayCharacterSound(SoundUtils.AnimStateToTrigger(animState));
    }

    private void SetUnitAngle()
	{
		switch (_animationState)
		{
			case E_UnitAnimationState.None:
			case E_UnitAnimationState.Idle:
			case E_UnitAnimationState.Idle2:
			case E_UnitAnimationState.Die:
			case E_UnitAnimationState.Defeat:
			case E_UnitAnimationState.Victory:
			case E_UnitAnimationState.Spawn:
			case E_UnitAnimationState.KnockBack:
			case E_UnitAnimationState.KnockDown:
			case E_UnitAnimationState.KnockDownLoop:
			case E_UnitAnimationState.Getup:
				if(_currentUnitData != null)
				{
					FVector2D anglePoint = FVector2D.Create(_target.position.x, _target.position.z);
                    FVector2D ownerPoint = FVector2D.Create(_currentGo.transform.position.x, _currentGo.transform.position.z);
					int gridAngle = MaestroMath.GetAngle(ownerPoint, anglePoint);
					_currentUnitData.SetCurrentAngle(gridAngle);
				}

				break;
		}
	}

	public void OnPointerDown()
	{
		if (_currentAnimator == null)
			return;

		_currentAnimator.speed = 0;
		AnimatorStateInfo state = _currentAnimator.GetCurrentAnimatorStateInfo(0);
		_currentAnimator.Play(state.shortNameHash, 0, _animationSlider.value);
		PlayParticleSimulate(true);
	}

	float prevDragValue = float.MinValue;
	public void OnDrag()
	{
		if (_currentAnimator == null)
			return;

		AnimatorStateInfo state = _currentAnimator.GetCurrentAnimatorStateInfo(0);
		_currentAnimator.Play(state.shortNameHash, 0, _animationSlider.value);
		PlayParticleSimulate(prevDragValue > _animationSlider.value ? true : false);
		prevDragValue = _animationSlider.value;
	}

	public void OnPointerUp()
	{
		if (_currentAnimator == null)
			return;

		if (_isPlay) _currentAnimator.speed = 1;

		PlayParticleSimulate(false);
	}

	public void OnClickPlayPause()
	{
		AnimationPlay(!_isPlay);
	}

	public void OnClickFxData(Button button)
	{
		Text fxText = button.GetComponentInChildren<Text>();
		string[] fxDataStrings = fxText.text.Split('\t');
		_toolFxDatas._fxStartAnimName.text = fxDataStrings[0];
		_toolFxDatas._fxStartTime.text = fxDataStrings[1];
		_toolFxDatas._fxPrefabName.text = fxDataStrings[2];
		_toolFxDatas._fxLifeTime.text = fxDataStrings[3];
		_toolFxDatas._fxRootName.text = fxDataStrings[4];
	}

	public void OnClickFxSave()
	{
		GameObject prefabObj = (GameObject)PrefabUtility.GetPrefabParent(_currentGo);

		ObjectAnimEffectComp comp = prefabObj.GetComponent<ObjectAnimEffectComp>();
		if (comp != null)
			DestroyImmediate(comp, true);

		ObjectAnimEffectComp newComp = prefabObj.AddComponent<ObjectAnimEffectComp>();
		if (newComp.AllDatas == null)
			newComp.AllDatas = new List<ObjectAnimEffectCompData>();
		for (int i = 0; i < _fxButtonDataList.Count; i++)
		{
			ObjectAnimEffectCompData compData = new ObjectAnimEffectCompData();
			compData.FxAnimationName = _fxButtonDataList[i].AnimStateName.ToLower();
			compData.FxLifeTime = _fxButtonDataList[i].FxLifeTime;
			compData.FxStartTime = _fxButtonDataList[i].FxStartTime;
			compData.FxPrefabName = _fxButtonDataList[i].FxPrefabName;
			compData.FxRootTransformName = _fxButtonDataList[i].FxRootName;

			newComp.AllDatas.Add(compData);
		}
	}

	public void OnClickProjectileSave()
	{
		GameObject prefabObj = (GameObject)PrefabUtility.GetPrefabParent(_currentGo);

		ObjectAnimProjectileComp comp = prefabObj.GetComponent<ObjectAnimProjectileComp>();
		if (comp != null)
			DestroyImmediate(comp, true);

		ObjectAnimProjectileComp newComp = prefabObj.AddComponent<ObjectAnimProjectileComp>();
		if (newComp.DataList == null)
			newComp.DataList = new List<ObjectAnimProjectileData>();
		for (int i = 0; i < _projectileButtonList.Count; i++)
		{
			ObjectAnimProjectileData compData = new ObjectAnimProjectileData();
			compData.AnimationName = _projectileButtonList[i].AnimName;
			compData.PrefabName = _projectileButtonList[i].PrefabName;
			newComp.DataList.Add(compData);
		}
	}

	#endregion

	private void PlayParticleSimulate(bool isRewind)
	{
		for (int i = 0; i < _fxPlayDataList.Count; i++)
		{
			if(_fxPlayDataList[i]._currentFxGo != null)
			{
				AutoDestroyTimer destoryTimer = _fxPlayDataList[i]._currentFxGo.GetComponent<AutoDestroyTimer>();
				if (destoryTimer != null)
					destoryTimer.Stop();
			}
			List<ParticleSystem> childParticleList = new List<ParticleSystem>(_fxPlayDataList[i]._currentParticleSystem.GetComponentsInChildren<ParticleSystem>());
			foreach(var particle in childParticleList)
			{
				particle.Pause(true);
				particle.randomSeed = 1;
			}
			if (_fxPlayDataList[i]._currentParticleSystem != null)
			{
				float fxTime = _currentAnimationTime - _fxPlayDataList[i]._fxStartTime;
				//_fxPlayDataList[i]._currentParticleSystem.Simulate(fxTime, true, isRewind);
				_fxPlayDataList[i]._currentParticleSystem.Simulate(fxTime, true, true);
			}

		}
	}

	private void SetFxData(string animName)
	{
		if (_fxPlayDataList == null)
			_fxPlayDataList = new List<FxToolEffectPlayData>();

		//for (int i = 0; i < _fxPlayDataList.Count; i++)
		//{
		//	Destroy(_fxPlayDataList[i]._currentFxGo);
		//}
		_fxPlayDataList.Clear();

		List<FxToolButtonData> buttonDatas = _fxButtonDataList.FindAll(x => x.AnimStateName.ToLower() == animName.ToLower());
		for (int i = 0; i < buttonDatas.Count; i++)
		{
			FxToolEffectPlayData playData = new FxToolEffectPlayData();
			playData._fxStartAnimName = buttonDatas[i].AnimStateName;
			playData._fxLifeTime = buttonDatas[i].FxLifeTime;
			playData._fxStartTime = buttonDatas[i].FxStartTime;
			playData._fxPrefabName = buttonDatas[i].FxPrefabName;
			playData._fxRootName = buttonDatas[i].FxRootName;

			_fxPlayDataList.Add(playData);
		}

		CreateFx();
	}

	#region Projectile

	private void StartProjectile(string animationName)
	{
		FxToolProjectileData projectileData = _skillProjectileLinkAnimationList.Find(data => data.animationName.ToLower().Equals(animationName.ToLower()));
		if(projectileData != null)
			StartCoroutine(CoStartProjectile(projectileData, _currentGo, _target.gameObject));
	}


	private Parabola3DObject _parabola = null;
	private SkillInfo _skillInfo = null;
	
	private IEnumerator CoStartProjectile(FxToolProjectileData data, GameObject unitObject, GameObject targetObject)
	{
		_parabola = null;
		yield return new WaitForSeconds(data.delay);

		ObjectHelperComp helper = unitObject.GetComponent<ObjectHelperComp>();
		if (helper == null)
			yield break;


		List<ObjectHelperData> helperDatas = helper.FindHelperData(ObjectHelperType.Projectile);
		if (helperDatas != null && helperDatas.Count > 0)
		{
			List<ObjectHelperData> tagHelperDatas = helperDatas.FindAll(a => a.Tag.ToLower().Equals(data.animationName.ToLower()));
			List<ObjectHelperData> defaultHelpderDatas = helperDatas.FindAll(a => string.IsNullOrEmpty(a.Tag));
			_skillInfo = Databases.SkillDB.GetInfo(data.skillName);
			if (_skillInfo.SkillType != E_SkillType.RangeCurveBunch)
			{
				GameObject fxObject = null;
				if (tagHelperDatas != null && tagHelperDatas.Count > 0)
				{
					fxObject = GameObject.Instantiate(data.projectileObj, tagHelperDatas[0].HelperTransform.position, Quaternion.identity) as GameObject;
				}
				else if(defaultHelpderDatas != null && defaultHelpderDatas.Count > 0)
				{
					fxObject = GameObject.Instantiate(data.projectileObj, defaultHelpderDatas[0].HelperTransform.position, Quaternion.identity) as GameObject;
				}
				var parabola = fxObject.GetComponent<Parabola3DObject>();
				if (parabola != null)
				{
					_parabola = parabola;
					ProjectileData projectile = new ProjectileData(null, _skillInfo, new FilterModifier(),
                        FVector3D.Create(_currentGo.transform.localPosition.x, _currentGo.transform.localPosition.y, _currentGo.transform.localPosition.z),
                        FVector3D.Create(_target.transform.localPosition.x, _target.transform.localPosition.y, _target.transform.localPosition.z), 0, 0, 0, E_Team.Home);
					_parabola.SetObjects(unitObject, targetObject);
					_parabola.SetData(projectile, Vector3.zero);
					_parabola.OnHit += OnHitParabola;
				}
			}
			else
			{
				for (int i = 0; i < 5; i++)
				{
					var fxObject = GameObject.Instantiate(data.projectileObj, helperDatas[0].HelperTransform.position, Quaternion.identity) as GameObject;
					var parabola = fxObject.GetComponent<Parabola3DObject>();
					ProjectileData projectile = new ProjectileData(null, _skillInfo, new FilterModifier(),
                        FVector3D.Create(_currentGo.transform.localPosition.x, _currentGo.transform.localPosition.y, _currentGo.transform.localPosition.z),
                        FVector3D.Create(_target.transform.localPosition.x, _target.transform.localPosition.y, _target.transform.localPosition.z), 0, 0, 0, E_Team.Home);
					projectile.SetBunchIndex(i);
					parabola.SetObjects(unitObject, targetObject);
					parabola.SetData(projectile, Vector3.zero);
					if (i == 0)
					{
						_parabola = parabola;
						parabola.OnHit += OnHitParabola;
					}
					else
					{
						parabola.SetBunchProjectile();
					}
					//_parabolaBunch.Add(parabola);
				}
			}
		}
	}
	private void OnHitParabola(Vector3 position)
	{
		EffectGenerator.Instance.PlayFX(_skillInfo.HitEffect, position, Quaternion.identity);
	}
	#endregion

	void PlayCharacterSound(E_SoundTrigger trigger)
	{
		if (_currentGo == null)
			return;

		CharacterSounds sounds = _currentGo.GetComponentInChildren<CharacterSounds>();
		if (sounds == null)
			return;
		CharacterSoundData[] soundData = sounds.Sounds;
		if (soundData != null && soundData.Length > 0)
		{
			for (int i = 0; i < soundData.Length; i++)
			{
				CharacterSoundData csData = soundData[i];
				if (csData.animState == trigger)
				{
					StartCoroutine(PlaySound(csData));
				}
			}
		}
	}

	IEnumerator PlaySound(CharacterSoundData data)
	{
		yield return new WaitForSeconds(data.delayTime);
		//SoundController.Instance.PostEvent(data.soundName);
		SoundController.Instance.PostEvent(data.eventID);

		yield return null;
	}

	public void OnClickSuperPlaySound()
	{
		PlayCharacterSound(E_SoundTrigger.Superplay);
	}

    public void OnClickSelectHero()
    {
        PlayCharacterSound(E_SoundTrigger.SelectHero);
    }

}
#endif