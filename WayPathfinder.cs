﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

class WayNodeSet
{
    public HashSet<WayPathNode> Nodes = new HashSet<WayPathNode>();
    public HashSet<WayPathEdge> Edges = new HashSet<WayPathEdge>();

    public void Clear()
    {
        if (Nodes != null)
            Nodes.Clear();
        if (Edges != null)
            Edges.Clear();
    }
}
public class WayPathfinder
{
    private static WayPathfinder _instance = null;
    public static WayPathfinder Instance
    {
        get
        {
            if (_instance == null)
                _instance = new WayPathfinder();
            return _instance;
        }
    }

    #region Node
    private NodeUtility _nodeUtil { get { return PathfindingManager.Instance.NodeUtil; } }

    private WayNodeSet _groundNode = new WayNodeSet();
    private WayNodeSet _limitedNode = new WayNodeSet();

    public HashSet<WayPathEdge> WayPathEdges { get { return _groundNode.Edges; } }
    public HashSet<WayPathNode> WayPathNodes { get { return _groundNode.Nodes; } }
    public HashSet<WayPathEdge> LimitWayPathEdges { get { return _limitedNode.Edges; } }
    public HashSet<WayPathNode> LimitWayPathNodes { get { return _limitedNode.Nodes; } }

    #endregion
    #region A*
    private int _trySearchMax = PathfindingConfig.TileTrySearchMax;
    private int _maxSearchRounds = 0;
    #endregion

    public void ClearPathNode()
    {
        if (_groundNode != null)
            _groundNode.Clear();
        if (_limitedNode != null)
            _limitedNode.Clear();
    }

    public WayPathNode AddWayPathNode(FVector2D fPos)
    {
        return AddWayPathNode(fPos.TileX, fPos.TileY);
    }

    public WayPathNode AddWayPathNode(int tileX, int tileZ)
    {
        int region = _nodeUtil.GetRegionNumber(tileX, tileZ);
        int id = _nodeUtil.GetId(tileX, tileZ);

        var tile = _nodeUtil.GetTileNode(tileX, tileZ);
        if (tile == null)
            Debug.LogError("Waypoint is on invalid position or tiles are not initialized.");
        WayPathNode node = new WayPathNode(tile);

        _groundNode.Nodes.Add(node);
        return node;
    }

    public WayPathNode AddWayLimitPathNode(FVector2D fPos)
    {
        return AddWayLimitPathNode(fPos.TileX, fPos.TileY);
    }

    public WayPathNode AddWayLimitPathNode(int tileX, int tileZ)
    {
        int region = _nodeUtil.GetRegionNumber(tileX, tileZ);
        int id = _nodeUtil.GetId(tileX, tileZ);

        var tile = _nodeUtil.GetTileNode(tileX, tileZ);
        if (tile == null)
            Debug.LogError("Waypoint is on invalid position or tiles are not initialized.");
        WayPathNode node = new WayPathNode(tile);

        _limitedNode.Nodes.Add(node);
        return node;
    }

    public void AddWayPathEdge(WayPathNode node1, WayPathNode node2, bool useLimitedArea)
    {
        WayPathEdge edge = new WayPathEdge(node1, node2);
        node1.AddEdge(edge);
        node2.AddEdge(edge);
        if (useLimitedArea)
            _limitedNode.Edges.Add(edge);
        else
            _groundNode.Edges.Add(edge);
    }
    public void RemoveWayPathNode(WayPathNode removedNode)
    {
        RemoveEdgesOf(removedNode);
        removedNode.ClearEdges();

        _groundNode.Nodes.Remove(removedNode);
        _limitedNode.Nodes.Remove(removedNode);
    }
    private void RemoveEdgesOf(WayPathNode removedNode)
    {
        var iter = removedNode.Edges.GetEnumerator();
        while (iter.MoveNext())
        {
            _groundNode.Edges.Remove(iter.Current);
            _limitedNode.Edges.Remove(iter.Current);
        }
    }
    private void ConstructEdgeOf(TileNode[,] targetTileNode, WayPathNode AddedNode, int size = 1, bool shouldEnsure = false, bool useLimitedArea = false)
    {
        int addedRegion = _nodeUtil.GetRegionNumber(AddedNode.PositionX, AddedNode.PositionZ);
        var iter = useLimitedArea ? _limitedNode.Nodes.GetEnumerator() : _groundNode.Nodes.GetEnumerator();
        while (iter.MoveNext())
        {
            WayPathNode node = iter.Current;
            int nodeRegion = _nodeUtil.GetRegionNumber(node.PositionX, node.PositionZ);
            if (AddedNode == node)
                continue;
            int startX = AddedNode.PositionX;
            int startZ = AddedNode.PositionZ;
            int endX = node.PositionX;
            int endZ = node.PositionZ;

            int checkingSize;
            if (shouldEnsure && addedRegion == nodeRegion) // 시작 지점과 끝 지점 추가시 같은 영역을 갈 경우 사이즈 체크를 하지 않아도 상관없음.
                checkingSize = 1;
            else if (shouldEnsure && addedRegion < 0) // 시작 지점과 끝 지점을 그릴 경우 영역이 아닌 위치에 있는 경우 사이즈 체크를 하도록.
                checkingSize = size;
            else
                checkingSize = size;

            bool isCollided = _nodeUtil.CheckCollisionOnStraightIgnoreSize(targetTileNode, startX, startZ, endX, endZ, checkingSize);
            if (isCollided == false)
                AddWayPathEdge(AddedNode, node, useLimitedArea);
        }
        if (AddedNode.Edges.Count == 0)
        {
            Debug.LogError("Edge Construction Failed!");
        }
    }

    public void ReConstructAllEdges()
    {
        var iter = _groundNode.Nodes.GetEnumerator();
        while (iter.MoveNext())
        {
            ConstructEdgeOf(_nodeUtil.OriginTileMap, iter.Current, 1, false, false);
        }

        iter = _limitedNode.Nodes.GetEnumerator();
        while (iter.MoveNext())
        {
            ConstructEdgeOf(_nodeUtil.OriginLimitedTileMap, iter.Current, 1, false, true);
        }
    }

    public void SetWayPathData(List<StaticWayPointData> wayPointList)
    {
        ClearPathNode();

        if (wayPointList != null)
        {
            for (int i = 0; i < wayPointList.Count; i++)
            {
                AddWayPathNode(wayPointList[i].Position);
                AddWayLimitPathNode(wayPointList[i].Position);
            }

            ReConstructAllEdges();
        }
    }

    #region map
    public void CreateTileMap(int tileCountX, int tileCountZ, TileData[] tiles)
    {
        //Set width and height
        _tileCountX = tileCountX;
        _tileCountZ = tileCountZ;
        _halfTileCountX = (int)(_tileCountX * 0.5f);
        _halfTileCountZ = (int)(_tileCountZ * 0.5f);

        if (PathfindingConfig.TileSize == 1)
        {
            _mapWidth = _tileCountX;
            _mapHeight = _tileCountZ;
        }
        else
        {
            _mapWidth = (int)(_tileCountX / PathfindingConfig.TileSize);
            _mapHeight = (int)(_tileCountZ / PathfindingConfig.TileSize);
        }

        int size = _mapWidth * _mapHeight;
        SetListsSize(size);
    }
    #endregion
    #region A*

    private WayPathNode[] _openList;
    private WayPathNode[] _closedList;
    private WayPathNode _startNode;
    private WayPathNode _endNode;
    private WayPathNode _currentNode;

    //Use it with KEY: F-value, VALUE: ID. ID's might be looked up in open and closed list then
    private List<NodeSearch> _sortedOpenList = new List<NodeSearch>();
    private int _tileCountX;
    private int _tileCountZ;
    private int _halfTileCountX;
    private int _halfTileCountZ;
    private int _mapWidth;
    private int _mapHeight;
    private int _heuristicAggression;

    /// <param name="useLimitedArea">Limit 영역 사용 가능 여부</param>
    /// <returns></returns>
    public bool FindPath(FVector2D startPos, FVector2D endPos, ref List<FVector2D> returnedPath, int size, bool useLimitedArea = false)
    {
        _startNode = null;
        _endNode = null;

        //Clear lists if they are filled
        Array.Clear(_openList, 0, _openList.Length);
        Array.Clear(_closedList, 0, _openList.Length);
        if (_sortedOpenList.Count > 0) { _sortedOpenList.Clear(); }

        TileNode[,] targetMap = useLimitedArea ? _nodeUtil.OriginLimitedTileMap : _nodeUtil.OriginTileMap;
        bool isValidStartEndPosition = SetStartAndEndNode(targetMap, startPos, endPos, size, useLimitedArea);
        if (isValidStartEndPosition == false)
            return false;
        InitializeWayPoints();

        //Insert start node
        _startNode.G = 0;
        _startNode.F = EstimateHeuristicCost(_startNode, _endNode);
        _openList[_startNode._id] = _startNode;
        BHInsertNode(new NodeSearch(_startNode._id, _startNode.F));

        bool endLoop = false;
        while (!endLoop)
        {
            //If we have no nodes on the open list AND we are not at the end, then we got stucked! return empty list then.
            if (_sortedOpenList.Count == 0)//If openList is empty, the pathfinding is failed.
            {
                RemoveStartAndEndNode();
                return false;
            }

            //Get lowest node and insert it into the closed list
            int id = BHGetLowest();
            _currentNode = _openList[id];
            if (_currentNode._id == _endNode._id)
            {
                endLoop = true;
                ReconstructPath(_currentNode, ref returnedPath, startPos);
                RemoveStartAndEndNode();

                continue;
            }

            _openList[id] = null;
            _closedList[_currentNode._id] = _currentNode;

            //Now look at neighbours, check for unwalkable tiles, bounderies, open and closed listed nodes.

            CheckNeighborWaypoints(_currentNode);

        }
        _maxSearchRounds = 0;
        return true;
    }
    private void SetListsSize(int size)
    {
        _openList = new WayPathNode[size];
        _closedList = new WayPathNode[size];
    }
    private void InitializeWayPoints()
    {
        var iter = _groundNode.Nodes.GetEnumerator();
        while (iter.MoveNext())
        {
            var node = iter.Current;
            node._parent = null;
            node.G = int.MaxValue;
            node.F = int.MaxValue;
        }
        iter = _limitedNode.Nodes.GetEnumerator();
        while (iter.MoveNext())
        {
            var node = iter.Current;
            node._parent = null;
            node.G = int.MaxValue;
            node.F = int.MaxValue;
        }
    }
    private void ReconstructPath(TileNode current, ref List<FVector2D> returnedPath, FVector2D startPos)
    {
        returnedPath.Clear();
        returnedPath.Add(FVector2D.Create(current.PositionX, current.PositionZ));
        while (current._parent != null)
        {
            current = current._parent;
            returnedPath.Add(FVector2D.Create(current.PositionX, current.PositionZ));

        }
        returnedPath.Reverse();
    }
    // Find start and end Node
    private bool SetStartAndEndNode(TileNode[,] targetTileMap, FVector2D start, FVector2D end, int size, bool useLimitedArea = false)
    {
        TileNode startTileNode = _nodeUtil.GetTileNode(targetTileMap, start.TileX, start.TileY);
        TileNode endTileNode = _nodeUtil.GetTileNode(targetTileMap, end.TileX, end.TileY);
        if (startTileNode == null || endTileNode == null)
            return false;
        if (startTileNode._walkable == false || startTileNode.canPassSize < size)
            startTileNode = FindNearestWalkableTileNode(startTileNode, size, 30);

        if (endTileNode._walkable == false || endTileNode.canPassSize < size)
            endTileNode = FindNearestWalkableTileNode(endTileNode, size, 30);

        if (startTileNode != null)
            _startNode = useLimitedArea ? AddWayLimitPathNode(startTileNode.PositionX, startTileNode.PositionZ) : AddWayPathNode(startTileNode.PositionX, startTileNode.PositionZ);
        if (endTileNode != null)
            _endNode = useLimitedArea ? AddWayLimitPathNode(endTileNode.PositionX, endTileNode.PositionZ) : AddWayPathNode(endTileNode.PositionX, endTileNode.PositionZ);

        ConstructEdgeOf(targetTileMap, _startNode, size, true, useLimitedArea);
        ConstructEdgeOf(targetTileMap, _endNode, size, true, useLimitedArea);

        if (_startNode == null || _endNode == null)
        {
            _maxSearchRounds = 0;
            return false;
        }
        return true;
    }

    private int cachingDepth = 1, cachingX = 0, cachingZ = 0;
    private TileNode FindNearestWalkableTileNode(TileNode tileNode, int size, int searchDepth)
    {
        TileNode resultNode = null;
        for (int depth = 1; depth < searchDepth; depth++)
        {
            for (int z = -depth; z <= depth; z++)
            {
                for (int x = -depth; x <= depth; x++)
                {
                    resultNode = _nodeUtil.GetTileNode(tileNode.PositionX + x, tileNode.PositionZ + z);
                    if (resultNode == null || resultNode._walkable == false || resultNode.canPassSize < size)
                        continue;

                    cachingDepth = depth;
                    cachingX = x;
                    cachingZ = z;
                    return resultNode;
                }
            }
        }
        return null;
    }

    private void RemoveStartAndEndNode()
    {
        RemoveWayPathNode(_startNode);
        RemoveWayPathNode(_endNode);
    }
    private void CheckNeighborWaypoints(WayPathNode node)
    {
        var iter = node.Edges.GetEnumerator();
        while (iter.MoveNext())        //for each node which connected with edges in this node
        {
            WayPathEdge edgeToNeighbor = iter.Current;
            WayPathNode neighbor = edgeToNeighbor.GetAnother(node);
            int j = neighbor.PositionX;
            int i = neighbor.PositionZ;

            bool isInClosedList = OnClosedList(neighbor._id);
            if (isInClosedList == true)//We do not recheck anything on the closed list
                continue;

            bool isInOpenList = OnOpenList(neighbor._id);
            NodeSearch addNodeSearch = null;

            if (isInOpenList == false)
            {
                _openList[neighbor._id] = neighbor;
                addNodeSearch = new NodeSearch(neighbor._id, neighbor.F);
                BHInsertNode(addNodeSearch);
            }
            int tentativeG = _currentNode.G + edgeToNeighbor.Distance;
            if (tentativeG >= neighbor.G)
                continue;//this is not better path

            neighbor._parent = _currentNode;
            neighbor.G = tentativeG;
            neighbor.F = neighbor.G + EstimateHeuristicCost(neighbor, _endNode);
            if (addNodeSearch != null)
                addNodeSearch.F = neighbor.F;
        }
    }
    //Check if a Node is already on the openList
    private bool OnOpenList(int id)
    {
        return (_openList[id] != null) ? true : false;
    }

    //Check if a Node is already on the closedList
    private bool OnClosedList(int id)
    {
        return (_closedList[id] != null) ? true : false;
    }

    private int EstimateHeuristicCost(TileNode startNode, TileNode endNode)
    {
        int h = MaestroMath.GetManhattanDistance(startNode.PositionX, startNode.PositionZ, endNode.PositionX, endNode.PositionZ);
        return h;
    }

    #region Binary_Heap (min)

    private void BHInsertNode(NodeSearch ns)
    {
        //We use index 0 as the root!
        if (_sortedOpenList.Count == 0)
        {
            _sortedOpenList.Add(ns);
            _openList[ns.ID].sortedIndex = 0;
            return;
        }

        _sortedOpenList.Add(ns);
        bool canMoveFurther = true;
        int index = _sortedOpenList.Count - 1;
        _openList[ns.ID].sortedIndex = index;

        while (canMoveFurther)
        {
            int parent = Mathf.FloorToInt((index - 1) / 2);

            if (index == 0) //We are the root
            {
                canMoveFurther = false;
                _openList[_sortedOpenList[index].ID].sortedIndex = 0;
            }
            else
            {
                if (_sortedOpenList[index].F < _sortedOpenList[parent].F)
                {
                    NodeSearch s = _sortedOpenList[parent];
                    _sortedOpenList[parent] = _sortedOpenList[index];
                    _sortedOpenList[index] = s;

                    //Save sortedlist index's for faster look up
                    _openList[_sortedOpenList[index].ID].sortedIndex = index;
                    _openList[_sortedOpenList[parent].ID].sortedIndex = parent;

                    //Reset index to parent ID
                    index = parent;
                }
                else
                {
                    canMoveFurther = false;
                }
            }
        }
    }

    private void BHSortNode(int id, int F)
    {
        bool canMoveFurther = true;
        int index = _openList[id].sortedIndex;
        _sortedOpenList[index].F = F;

        while (canMoveFurther)
        {
            int parent = Mathf.FloorToInt((index - 1) / 2);

            if (index == 0) //We are the root
            {
                canMoveFurther = false;
                _openList[_sortedOpenList[index].ID].sortedIndex = 0;
            }
            else
            {
                if (_sortedOpenList[index].F < _sortedOpenList[parent].F)
                {
                    NodeSearch s = _sortedOpenList[parent];
                    _sortedOpenList[parent] = _sortedOpenList[index];
                    _sortedOpenList[index] = s;

                    //Save sortedlist index's for faster look up
                    _openList[_sortedOpenList[index].ID].sortedIndex = index;
                    _openList[_sortedOpenList[parent].ID].sortedIndex = parent;

                    //Reset index to parent ID
                    index = parent;
                }
                else
                {
                    canMoveFurther = false;
                }
            }
        }
    }

    private int BHGetLowest()
    {

        if (_sortedOpenList.Count == 1) //Remember 0 is our root
        {
            int ID = _sortedOpenList[0].ID;
            _sortedOpenList.RemoveAt(0);
            return ID;
        }
        else if (_sortedOpenList.Count > 1)
        {
            //save lowest not, take our leaf as root, and remove it! Then switch through children to find right place.
            int ID = _sortedOpenList[0].ID;
            _sortedOpenList[0] = _sortedOpenList[_sortedOpenList.Count - 1];
            _sortedOpenList.RemoveAt(_sortedOpenList.Count - 1);
            _openList[_sortedOpenList[0].ID].sortedIndex = 0;

            int index = 0;
            bool canMoveFurther = true;
            //Sort the list before returning the ID
            while (canMoveFurther)
            {
                int child1 = (index * 2) + 1;
                int child2 = (index * 2) + 2;
                int switchIndex = -1;

                if (child1 < _sortedOpenList.Count)
                {
                    switchIndex = child1;
                }
                else
                {
                    break;
                }
                if (child2 < _sortedOpenList.Count)
                {
                    if (_sortedOpenList[child2].F < _sortedOpenList[child1].F)
                    {
                        switchIndex = child2;
                    }
                }
                if (_sortedOpenList[index].F > _sortedOpenList[switchIndex].F)
                {
                    NodeSearch ns = _sortedOpenList[index];
                    _sortedOpenList[index] = _sortedOpenList[switchIndex];
                    _sortedOpenList[switchIndex] = ns;

                    //Save sortedlist index's for faster look up
                    _openList[_sortedOpenList[index].ID].sortedIndex = index;
                    _openList[_sortedOpenList[switchIndex].ID].sortedIndex = switchIndex;

                    //Switch around idnex
                    index = switchIndex;
                }
                else
                {
                    break;
                }
            }
            return ID;

        }
        else
        {
            return -1;
        }
    }

    #endregion
    #endregion
}
