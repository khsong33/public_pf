﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Maestro;

public class HUDBase : MonoBehaviour 
{
	public int GenerateID = -1;
	public E_HUDType HUDType { get; protected set; }

	[SerializeField]
	protected List<GameObject> _hudPivotRoot = null;

	protected GameObject _targetObject = null;
	protected List<GameObject> _targetObjects = null;

	protected Camera _worldCamera = null;
	protected float _pixelHeightScale = 0.0f;

	protected UIRoot _uiRoot;
	protected int _offsetOrder;

	[SerializeField]
	protected float _scaleValue = 80.0f;
	[SerializeField]
	protected float _scaleValuePlayView = 80.0f;
	[SerializeField]
	protected float _scaleValueStrategyView = 80.0f;
	//[SerializeField]
	//protected UILabel _testHpLabel;

	protected Color hudRed = new Color(1.0f, 0.349019607f, 0.160784313f, 1.0f);

	private ObjectLayerComponent _layerComponent;

	private bool _isStrategyView = true;
	private bool _isInitCameraState = false;

	private bool _isActive = true;
	public virtual void InitHUD()
	{
		_uiRoot = NGUITools.FindInParents<UIRoot>(gameObject);
		_offsetOrder = (int)(_uiRoot.activeHeight * 0.5f);
		if(_hudPivotRoot == null || _hudPivotRoot.Count <= 0)
		{
			_hudPivotRoot.Add(gameObject);
		}
		_isInitCameraState = false;
	}

	public virtual void OnDestoryObject()
	{
			
	}

    public virtual void Show(bool enable)
    {
		if (GameManager.Instance.BattleHUD == null)
		{
			gameObject.SafeSetActive(false);
			_isActive = enable;
		}
		else
		{
			gameObject.SafeSetActive(enable);
			_isActive = enable;
		}
	}

	public virtual void SetTargets(List<GameObject> targets)
	{
		_targetObjects = targets;
		//if(_targetObjects.Count > _hudPivotRoot.Count)
		//{
		//	for(int i = _hudPivotRoot.Count; i < _targetObjects.Count; i++)
		//		_hudPivotRoot.Add(gameObject);
		//}
	}
	
	public virtual void SetWorldCamera(Camera camera)
	{
		_worldCamera = camera;
	}

	public virtual void SetHUDType(E_HUDType type)
	{
		HUDType = type;
	}

	public virtual void SetCameraState(bool isStrategyView, float duration)
	{
		if(gameObject.activeInHierarchy && (_isStrategyView != isStrategyView || _isInitCameraState == false))
			StartCoroutine(CoSetCameraState(isStrategyView, duration));

		_isStrategyView = isStrategyView;
		_isInitCameraState = true;
	}

	private IEnumerator CoSetCameraState(bool isStrategyView, float duration)
	{
		float currentTime = 0;
		float from = isStrategyView ? _scaleValuePlayView : _scaleValueStrategyView;
		float to = isStrategyView ? _scaleValueStrategyView : _scaleValuePlayView;
		while(currentTime <= duration)
		{
			float factor = currentTime / duration;
			_scaleValue = Mathf.Lerp(from, to, factor);

			currentTime += Time.deltaTime;
			yield return null;
		}
		_scaleValue = to;
	}

	public virtual void HUDUpdate()
	{
		if (_isActive == false)
			return;

		if(_targetObjects != null && _worldCamera != null)
		{
			for(int i = 0; i < _targetObjects.Count; i++)
			{
				if (_hudPivotRoot.Count <= i)
					break;
				var target = _targetObjects[i];
				if (target == null)
					break;
				Vector3 worldScreenPos = _worldCamera.WorldToScreenPoint(target.transform.position);
				Vector3 hudPosition = UICamera.mainCamera.ScreenToWorldPoint(worldScreenPos);
				_hudPivotRoot[i].transform.position = new Vector3(hudPosition.x, hudPosition.y, 0.0f);

				// Show/Hide로 인한 Coroutine 타이밍 이슈로 인해 예외처리 추가 
				if(float.IsNaN(_scaleValue))
				{
					_scaleValue = _isStrategyView ? _scaleValuePlayView : _scaleValueStrategyView;
				}

				_pixelHeightScale = (_scaleValue / (GameManager.Instance.BattleCamera._battleCamera.GetCamHeight()));
				if (_pixelHeightScale > 0)
					_hudPivotRoot[i].transform.localScale = new Vector3(1f * _pixelHeightScale, 1f * _pixelHeightScale, 1.0f);
			}
		}
	}

}
