﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class ActionCamManager
{
	private static ActionCamManager _instance = null;
	public static ActionCamManager Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new ActionCamManager();
			}
			return _instance;
		}
	}

	public BattleCameraBase BattleCamera { get; private set; }
	public ActionCamBase ActionCam { get; private set; }
	public void SetBattleCamera(BattleCameraBase battleCamera)
	{
		BattleCamera = battleCamera;
	}

	public GameObject Load(string actionCamName)
	{
		if (ActionCam != null)
			Stop(false);

		GameObject ActionCamObj = AssetBundleManager.Instance.LoadAssetInstantiate<GameObject>("actioncams", actionCamName);
		if (ActionCamObj == null)
			return null;

        ActionCamObj.name = actionCamName;
        ActionCam = ActionCamObj.GetComponent<ActionCamBase>();
		if (ActionCam == null)
			return null;

        return ActionCamObj;
	}

	public void Play()
	{
		ActionCam.PlayActionCam();

		if (ActionCam == null)
			return;
		if(BattleCamera != null)
		{
			BattleCamera.gameObject.SetActive(false);
		}
	}

	public void Stop(bool isBattleCamOn = true)
	{
		if (ActionCam == null)
			return;

		ActionCam.StopActionCam();

		GameObject.Destroy(ActionCam.gameObject);
		ActionCam = null;

		if (BattleCamera != null && isBattleCamOn)
			BattleCamera.gameObject.SetActive(true);
	}

    public void SetBlur(bool enable)
    {
        if (ActionCam == null) return;

        BlurOptimized blur = ActionCam.ActionCamera.GetComponent<BlurOptimized>();
        if(blur != null)
        {
            blur.enabled = enable;
        }
    }
}
 